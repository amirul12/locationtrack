package com.mandsti.amirul.locationtracker.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.widget.TextView;

import com.mandsti.amirul.locationtracker.Util.GpsTracker;

public class MainActivityPresenter {

    private Activity activity;

    private ProgressDialog pd;

    private GpsTracker gpsTracker;

    /**
     *
     * @param activity
     */
    public MainActivityPresenter(Activity activity) {
        this.activity = activity;
    }


    /**
     * getting location
     * @param lan
     * @param lon
     */
    public void getLocation(final TextView lan, TextView lon) {
        gpsTracker = new GpsTracker(activity);
        if (gpsTracker.canGetLocation()) {
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

            lan.setText(String.valueOf(latitude));
            lon.setText(String.valueOf(longitude));
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    /*show progress dialog*/
    public void intProgressbar() {

        pd = new ProgressDialog(activity);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pd.getWindow().setGravity(Gravity.CENTER_HORIZONTAL);
        TextView tv = new TextView(activity);
        tv.setTextColor(Color.WHITE);
        tv.setTextSize(20);
        tv.setText("Waiting...");
        pd.setCustomTitle(tv);
        pd.setIndeterminate(true);
        pd.show();


    }

    /*hide dialog*/
    public void hideProgress() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    /*show dialog*/
    public void showProgress() {
        if (pd == null) {
            pd.show();
        }
    }


}
