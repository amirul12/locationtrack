package com.mandsti.amirul.locationtracker.Util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.mandsti.amirul.locationtracker.R;
import com.mandsti.amirul.locationtracker.model.Notification;
import com.mandsti.amirul.locationtracker.adapter.NotificationAdapter;

import java.util.ArrayList;
import java.util.List;

public class DailogUtilClass {

    Activity activity;

    public DailogUtilClass(Activity activity) {
        this.activity = activity;
    }




    public void notificationMessage(){

        final Dialog dialog = new Dialog(activity);
       // dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.fragment_notification);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            dialog.getWindow().setStatusBarColor(Color.CYAN);
        }

        final Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.gravity = Gravity.TOP; //psotion
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; // fuill screen
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogWindow.setAttributes(lp);


        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id

        dialog.setCancelable(true);

        RecyclerView recyclerView = (RecyclerView)dialog.findViewById(R.id.recyNotification);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(manager);
        List<Notification> notificationList = new ArrayList<>();

        notificationList.add(new Notification("Notification","It is a long established fact that a reader will be distracted Ipsum is tha"));
        notificationList.add(new Notification("Notification","It is a long established fact that a reader will be distracted Ipsum is tha"));
        notificationList.add(new Notification("Notification","It is a long established fact that a reader will be distracted Ipsum is tha"));

        NotificationAdapter adapter = new NotificationAdapter(activity,notificationList);
        recyclerView.setAdapter(adapter);

      /*  ImageButton btnCross = (ImageButton) dialog.findViewById(R.id.btnCross);
        btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });*/

        dialog.show();
    }



    public void showAlert(final Context context) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }









}
