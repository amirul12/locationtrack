package com.mandsti.amirul.locationtracker.view;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mandsti.amirul.locationtracker.R;
import com.mandsti.amirul.locationtracker.Util.DailogUtilClass;
import com.mandsti.amirul.locationtracker.Util.GpsTracker;
import com.mandsti.amirul.locationtracker.presenter.MainActivityPresenter;

public class MainActivity extends AppCompatActivity  {


   private TextView latitude, longitude;
    Button showResult;
    ImageButton btn_back;
    MainActivityPresenter presenter;
    private GpsTracker gpsTracker;
    ImageButton imgBtnNotification;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DailogUtilClass dialo = new DailogUtilClass(MainActivity.this);
        presenter = new MainActivityPresenter(this);


        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        gpsTracker = new GpsTracker(MainActivity.this);

        btn_back =(ImageButton)findViewById(R.id.btn_back);
        latitude = (TextView) findViewById(R.id.tv_latitude);
        latitude.setText("00.00000");
        longitude = (TextView) findViewById(R.id.tv_longitude);
        longitude.setText("00.00000");
        showResult = (Button) findViewById(R.id.btn_show_result);

        imgBtnNotification = (ImageButton) findViewById(R.id.imgBtnNotification);

        /*notification show*/
        imgBtnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialo.notificationMessage();
            }
        });

        /*back button */
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /*show result */
        showResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               presenter.intProgressbar();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms

                        presenter.hideProgress();

                        presenter.getLocation(latitude, longitude);

                    }
                }, 2000);


            }
        });


    }




}
